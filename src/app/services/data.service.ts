import { Injectable } from '@angular/core';

import { NavigationStart, Router } from '@angular/router';

import { RestApiService } from './rest-api.service';

@Injectable()
export class DataService {
  message = '';
  messageType = 'danger';

  user: any;

  constructor(private router: Router, private rest: RestApiService) {
    // If the route changes, we clear the message variable
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.message = '';
      }
    });
  }

  public error(message) {
    this.messageType = 'danger';  // For bootstrap
    this.message = message;
  }

  public success(message) {
    this.messageType = 'success'; // For bootstrap
    this.message = message;
  }

  public warning(message) {
    this.messageType = 'warning'; // For bootstrap
    this.message = message;
  }

  public async getProfile() {
    try {
      if (localStorage.getItem('token')) {
        const data = await this.rest.get('http://localhost:3030/api/accounts/profile');
        this.user = data['user'];
      }
    } catch (error) {
      this.error(error);
    }
  }
}
