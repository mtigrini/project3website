import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { RestApiService } from '../services/rest-api.service';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit {
  public users;
  constructor(private data: DataService,  private rest: RestApiService) {
    if (this.data.user.isAdministrator) {
     this.getUsers().then((users) => {
       this.users = users;
       console.log(this.users);
     });
    }
   }

  async getUsers() {
    const data = await this.rest.post('http://localhost:3030/api/accounts/usersList',
    {
      isAdministrator: this.data.user.isAdministrator,
    });
    console.log(data);
    return data['users'];
  }

  ngOnInit() {
  }

  async changeRights(user) {
    const res = await this.rest.post('http://localhost:3030/api/accounts/profile',
    {
      query: 'updateRights',
      _id: user._id,
      isAdministrator: !user.isAdministrator,
    });
    console.log(res);
  }
}
